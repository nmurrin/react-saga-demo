# docker-spring-react-demo
To run these commands you will need docker installed.
Some installs (linux installs) will require you to install
docker-compose and docker-machine separately.

# Run local mongo server (needed for application)
# -d = daemon mode (run in background)
docker pull mongo
docker run --name mongo -d -p 27017:27017 mongo

# Run docker ps to see running docker processes (you should see mongo)
docker ps

# Build local docker dev image which will run the application
# This command uses the Dockerfile to build an image
docker build -t docker-demo:1.0 .

# Run the npm install locally to load javascript modules
npm install

# Run my docker dev image
# Browse to http://localhost:8080/ after running
# The -v argument allows the container to see / run code on your machine
# Replace with the path to this project on your machine
# -it = interactive with terminal (can use -d instead to run in foreground)
# If your on Windows:
#   Volume mapping will be something like '-v c:\workspace\docker-spring-react-demo:/app'
docker run --name docker-demo -d -p 8080:8080 -v /home/norm/Workspace/docker-spring-react-demo:/app \
--link mongo -w /app docker-demo:1.0

# Bring up a shell in docker containiner
# Open a new terminal and run the following.
# Just type 'exit' to close the shell when done
docker exec -it docker-demo /bin/bash

# You can run other processes in the container (run each in a new terminal)
# The processes below will watch Java / Javascript files for changes
# This allows you to change code and see changes without stopping / restarting
docker exec -it -w /app docker-demo /app/gradlew build -continuous
docker exec -it -w /app docker-demo npm start

# Stop everything
docker stop docker-demo
docker stop mongo
# The stopped images still exist and can be started or removed
# View with:
docker ps -a
# To completely remove:
docker rm docker-demo
docker rm mongo

# Docker Compose - convenient way to start related containers
# This will start our app and a mongo container
# These commands use the docker-compose.yml file
# Browse to http://localhost:8080/ after containers start
docker-compose up
ctrl-c to stop

# Can also run containers in background
docker-compose up -d
docker-compose down

# You will need a few things if you want to run this last part
# - a Digital Ocean account and API key for this next section
# - gradle installed on your local server
# The cost to run this will be minimal
# THESE COMMANDS WILL NOT WORK BEHIND A CORPORATE FIREWALL

# Create a digital ocean droplet with docker server
# You can sign-in to digital ocean and see the machine being created
# and the IP address assigned!
docker-machine create --driver digitalocean --digitalocean-access-token \
ad66ee2b95ce9f799b77514ff061167e4585f6b0f0e531877712fa52f76aa354 docker-2

docker-machine env docker-1
eval $(docker-machine env docker-1)

# Builds an image with jar instead of code (more like a prod deployment)
# This build is setup in build.gradle and DockerfileGradle
./gradlew assemble
./gradlew build
./gradlew docker

# Run mongo and your app on the remote server
docker run --name mongo -d mongo
docker run -d -p 80:8080 --link mongo com.norm/docker-demo

# YOU JUST DEPLOYED YOUR APPLICATION!
# At this time, retrieve the IP address for your server from digital ocean
# Browse to http://[MACHINE_IP]/

# When you're done, remove the machine and set docker client to local again
docker-machine rm docker-1
docker-machine env --unset
eval $(docker-machine env --unset)


