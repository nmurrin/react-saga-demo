FROM openjdk:8-jdk
VOLUME /tmp
VOLUME /app

RUN apt-get update
RUN apt-get install curl -y

# Install Gradle
RUN curl -L https://services.gradle.org/distributions/gradle-2.4-bin.zip -o gradle-2.4-bin.zip
RUN apt-get install -y unzip
RUN unzip gradle-2.4-bin.zip
ENV GRADLE_HOME=/app/gradle-2.4
ENV PATH=$PATH:$GRADLE_HOME/bin

# Install Node
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - && apt-get install nodejs -y
RUN npm install webpack@3.11.0 -g

ENV SPRING_PROFILES_ACTIVE=docker

WORKDIR /app
CMD npm install && ./gradlew bundleJs bootRun 
