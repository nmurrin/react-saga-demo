# docker-spring-react-demo
To run these commands you will need docker installed.
Some installs (linux installs) will require you to install
docker-compose and docker-machine separately.

# Run local mongo server (needed for application)
# -d = daemon mode (run in background)
docker pull mongo
docker run --name mongo -d -p 27017:27017 mongo

# Run docker ps to see running docker processes (you should see mongo)
docker ps

# Run the npm install locally to load javascript modules
npm install

# Run the server api java application
./gradlew bootRun

# Run the JavaScript React application
npm run start:dev
