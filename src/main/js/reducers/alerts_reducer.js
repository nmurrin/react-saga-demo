import {
    SUCCESS_ALERT_SAVE, SUCCESS_ALERT_CLEAR, ERROR_ALERT_SAVE, ERROR_ALERT_CLEAR, LOADING
} from '../actions';

export default function (state = {}, action) {
    switch (action.type) {
        case SUCCESS_ALERT_SAVE:
            return { ...state, successAlert: action.message };
        case SUCCESS_ALERT_CLEAR:
            return { ...state, successAlert: null };
        case ERROR_ALERT_SAVE:
            return { ...state, errorAlert: action.message };
        case ERROR_ALERT_CLEAR:
            return { ...state, errorAlert: null };
        case LOADING:
            return { ...state, loading: action.loading };
        default:
            return state;
    }
}
