import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import AlertsReducer from './alerts_reducer'
import TodosReducer from './todos_reducer';

const rootReducer = combineReducers({
  alerts: AlertsReducer,
  todos: TodosReducer,
  form: formReducer
});

export default rootReducer;
