import _ from 'lodash'
import { FETCH_TODOS_RESPONSE, FETCH_TODO_RESPONSE, CREATE_TODO_RESPONSE, DELETE_TODO_RESPONSE } from '../actions';

export default function (state = {}, action) {
    switch (action.type) {
        case FETCH_TODOS_RESPONSE:
            return { ...state, ['list']: action.payload };
        case FETCH_TODO_RESPONSE:
            return { ...state, [action.payload.id]: action.payload };
        case CREATE_TODO_RESPONSE:
            return {...state, [action.payload.id]: action.payload };
        case DELETE_TODO_RESPONSE:
            return _.omit(state, action.payload);
        default:
            return state;
    }
}
