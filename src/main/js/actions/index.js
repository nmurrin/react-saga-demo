export const FETCH_TODOS = 'fetch_todos';
export const FETCH_TODOS_RESPONSE = 'fetch_todos_response';
export const FETCH_TODO = 'fetch_todo';
export const FETCH_TODO_RESPONSE = 'fetch_todo_response';
export const CREATE_TODO = 'create_todo';
export const CREATE_TODO_RESPONSE = 'create_todo_response';
export const DELETE_TODO = 'delete_todo';
export const DELETE_TODO_RESPONSE = 'delete_todo_response';
export const SUCCESS_ALERT = 'success_alert';
export const SUCCESS_ALERT_SAVE = 'success_alert_save'
export const SUCCESS_ALERT_CLEAR = 'success_alert_clear';
export const ERROR_ALERT = 'error_alert';
export const ERROR_ALERT_SAVE = 'error_alert_save'
export const ERROR_ALERT_CLEAR = 'error_alert_clear';
export const APP_METRIC = "app_metric";
export const LOADING = "loading";

export function fetchTodos() {
    return {
        type: FETCH_TODOS
    };
}

export function fetchTodo(id) {
    return {
        type: FETCH_TODO,
        id: id,
    }
}

export function createTodo(values, callback) {
    return {
        type: CREATE_TODO,
        values,
        callback
    };
}

export function deleteTodo(id, callback) {
    return {
        type: DELETE_TODO,
        id,
        callback
    };
}

export function successAlert(message) {
    return {
        type: SUCCESS_ALERT,
        message
    };
}

export function successAlertSave(message) {
    return {
        type: SUCCESS_ALERT_SAVE,
        message
    };
}

export function successAlertClear(message) {
    return {
        type: SUCCESS_ALERT_CLEAR
    }
}

export function errorAlert(message) {
    return {
        type: ERROR_ALERT,
        message
    };
}

export function errorAlertSave(message) {
    return {
        type: ERROR_ALERT_SAVE,
        message
    };
}

export function errorAlertClear(message) {
    return {
        type: ERROR_ALERT_CLEAR
    }
}

export function appMetric(metric) {
    return {
        type: APP_METRIC,
        metric
    }
}

export function loading(loading) {
    return {
        type: LOADING,
        loading
    }
}
