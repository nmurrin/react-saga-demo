import "regenerator-runtime/runtime";
import { takeEvery, call, put, fork, all, delay } from "redux-saga/effects";
import axios from "axios";
import * as actions from '../actions'
import { FETCH_TODOS_RESPONSE, FETCH_TODO_RESPONSE, CREATE_TODO_RESPONSE, DELETE_TODO_RESPONSE } from '../actions';
import {successAlert} from "../actions";
import {clearAlert} from "../actions";

const ROOT_URL = '/api';

export function* watchAppMetrics() {
    yield takeEvery(actions.APP_METRIC, appMetricSaga);
}

export function* appMetricSaga(action) {
    const response = yield call(axios.post, `${ROOT_URL}/metrics/${action.metric}`);
}
