import "regenerator-runtime/runtime";
import { fork } from "redux-saga/effects";
import { watchCreateTodo, watchDeleteTodo, watchFetchTodo, watchFetchTodos } from "./todos_saga";
import { watchSuccessAlert, watchErrorAlert } from "./alerts_saga";
import { watchAppMetrics } from "./metrics_saga";

export default function* rootSaga() {
    yield fork(watchFetchTodos)
    yield fork(watchFetchTodo)
    yield fork(watchCreateTodo)
    yield fork(watchDeleteTodo)
    yield fork(watchSuccessAlert)
    yield fork(watchErrorAlert)
    yield fork(watchAppMetrics)
}
