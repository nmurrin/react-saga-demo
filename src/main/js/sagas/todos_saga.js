import "regenerator-runtime/runtime";
import { takeEvery, call, put, spawn } from "redux-saga/effects";
import axios from "axios";
import * as actions from '../actions'
import { FETCH_TODOS_RESPONSE, FETCH_TODO_RESPONSE, CREATE_TODO_RESPONSE, DELETE_TODO_RESPONSE } from '../actions';
import { successAlert, errorAlert } from "../actions";
import { appMetric } from "../actions";
import { appMetricSaga } from "./metrics_saga";
import {loading} from "../actions";

const ROOT_URL = '/api';

export function* watchFetchTodos() {
    yield takeEvery(actions.FETCH_TODOS, fetchTodosSaga);
}

export function* watchFetchTodo() {
    yield takeEvery(actions.FETCH_TODO, fetchTodoSaga);
}

export function* watchCreateTodo() {
    yield takeEvery(actions.CREATE_TODO, createTodoSaga);
}

export function* watchDeleteTodo() {
    yield takeEvery(actions.DELETE_TODO, deleteTodoSaga);
}

export function* fetchTodosSaga(action) {
    yield put(loading(true));
    yield spawn(appMetricSaga, appMetric("API: Fetch Todos"))
    const response = yield call(axios.get, `${ROOT_URL}/todo/`);
    const payload = response ? response.data : {}
    yield put({ type: FETCH_TODOS_RESPONSE, payload });
    yield put(loading(false));
}

export function* fetchTodoSaga(action) {
    yield put(loading(true));
    yield put(appMetric("API: Fetch Todo"))
    const response = yield call(axios.get, `${ROOT_URL}/todo/${action.id}`);
    const payload = response ? response.data : {}
    yield put({ type: FETCH_TODO_RESPONSE, payload });
    yield put(loading(false));
}

export function* createTodoSaga(action) {
    try {
        yield put(appMetric("API: Create Todo"))
        const response = yield call(axios.post, `${ROOT_URL}/todo/`, action.values);
        action.callback();
        const payload = response ? response.data : {}
        yield put({type: CREATE_TODO_RESPONSE, payload});
        yield put(successAlert("Todo successfuly saved!"))
    } catch (x) {
        yield put(errorAlert("An error occurred, please try again!"))
    }
}

export function* deleteTodoSaga(action) {
    yield put(appMetric("API: Delete Todo"))
    const response = yield call(axios.delete, `${ROOT_URL}/todo/${action.id}`);
    action.callback();
    const payload = response ? response.data : {}
    yield put({ type: DELETE_TODO_RESPONSE, payload: action.id });
    yield put(successAlert("Todo successfuly deleted!"))
}
