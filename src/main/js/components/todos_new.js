import React, { Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createTodo, appMetric } from '../actions';
import Alerts from './alerts'

class TodosNew extends Component {

    componentDidMount() {
        this.props.appMetric("PAGE: New Todo")
    }

    renderField(field) {
        const { meta: {touched, error} } = field;
        const className = `form-group ${touched && error ? 'has-danger' : ''}`;
        return (
            <div className={className}>
                <label>{field.label}</label>
                <input
                    className="form-control"
                    type="text"
                    {...field.input}
                />
                <div className="text-help">
                    {touched ? error : ''}
                </div>
            </div>
        )
    }

    onSubmit(values) {
        this.props.createTodo(values, () => {
            this.props.history.push('/');
        });
    }
    render() {
        const { handleSubmit } = this.props;

        return (
            <div>
                <Alerts />
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                    <Field
                        name="task"
                        component={this.renderField}
                        label="Task"
                    />
                    <Field
                        name="description"
                        component={this.renderField}
                        label="Description"
                    />
                    <button type="submit"  className="btn btn-primary">Submit</button>
                    <Link to="/" className="btn btn-danger">Cancel</Link>

                </form>
            </div>
        );
    };
}

function validate(values) {
    const errors = {};

    if (!values.task) {
        errors.task = "Enter a task!";
    }

    if (!values.description) {
        errors.description = "Enter a description!";
    }

    return errors;
}

export default reduxForm({
    validate,
    form: 'TodosNewForm'
})(
    connect(null, { createTodo, appMetric })(TodosNew)
);