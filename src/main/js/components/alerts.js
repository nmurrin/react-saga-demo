import React, { Component } from 'react';
import { connect } from 'react-redux';

class Alerts extends Component {
  render() {
    return (
      <div>
        {this.props.loading &&
          <div>Loading...</div>
        }

        {this.props.successAlert &&
          <div className="alert alert-success" role="alert">
              {this.props.successAlert}
          </div>
        }

        {this.props.errorAlert &&
          <div className="alert alert-danger" role="alert">
              {this.props.errorAlert}
          </div>
        }
      </div>
    );
  }
}

function mapStateToProps({ alerts }, ownProps ) {
  return {
      successAlert: alerts.successAlert,
      errorAlert: alerts.errorAlert,
      loading: alerts.loading
  };
}
export default connect(mapStateToProps)(Alerts);
