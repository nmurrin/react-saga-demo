import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchTodos, appMetric } from '../actions';
import Alerts from './alerts'

class Todos extends Component {

  componentDidMount() {
    this.props.fetchTodos();
    this.props.appMetric("PAGE: Home")
  }

  renderTodos() {
    return this.props.todos.list.map((todo) => {
      return (
        <tr key={todo.id}>
          <td><Link to={`/todos/${todo.id}`}>{todo.task}</Link></td>
          <td>{todo.description}</td>
        </tr>
      )
    })
  }

  render() {

    return (
      <div>
        <Alerts />

        <div className="text-xs-right">
          <Link className="btn btn-primary" to="/todos/new">
            Add a Todo
          </Link>
        </div>

        <h3>Todos</h3>

        <table className="table table-striped table-hover"> 
          <thead className="thead-dark">
            <tr>
              <th>Task</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
          {this.props.todos.list && this.renderTodos()}
          </tbody>
        </table>

      </div>
    );
  }
}

function mapStateToProps({ todos }, ownProps ) {
  return {
      todos
  };
}
export default connect(mapStateToProps, { fetchTodos, appMetric })(Todos);
