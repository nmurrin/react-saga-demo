package com.norm.todo.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("${spring.data.rest.base-path}")
public class MetricsController {

    private static HashMap<String, Integer> metrics = new HashMap();

    @RequestMapping("/metrics")
    public HashMap<String, Integer> getMetrics() {
        return metrics;
    }

    @RequestMapping(value = "/metrics/{metric}", method = RequestMethod.POST)
    public void saveMetric(@PathVariable("metric") String metric) {
        if (metrics.containsKey(metric)) {
            metrics.put(metric, metrics.get(metric) + 1);
        } else {
            metrics.put(metric, 1);
        }
        try {
            Thread.sleep(5000);
        } catch (Exception x) {}
    }
}