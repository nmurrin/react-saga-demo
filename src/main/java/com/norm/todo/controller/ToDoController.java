package com.norm.todo.controller;

import java.util.List;

import com.norm.todo.model.ToDo;
import com.norm.todo.repository.ToDoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("${spring.data.rest.base-path}")
public class ToDoController {

    @Autowired
    ToDoRepository todoRepository;

    @RequestMapping("/todo") 
    public List<ToDo> getToDoList() {
        try {
            Thread.sleep(1000);
        } catch (Exception x) {}
        return todoRepository.findAll();
    }

    @RequestMapping("/todo/{id}") 
    public ToDo getToDo(@PathVariable("id") String todoId) {
        try {
            Thread.sleep(1000);
        } catch (Exception x) {}
        return todoRepository.findById(todoId).get();
    }

    @RequestMapping(value = "/todo", method = RequestMethod.POST)
    public ToDo saveToDo(@RequestBody ToDo todo) {
        if (todo.getDescription().equalsIgnoreCase("error")) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "An error occurred");
        }

        ToDo saved = todoRepository.save(todo);
        return saved;
    }

    @RequestMapping(value = "/todo/{id}", method = RequestMethod.DELETE)
    public void deletToDo(@PathVariable("id") String todoId) {
        todoRepository.deleteById(todoId);
    }
}