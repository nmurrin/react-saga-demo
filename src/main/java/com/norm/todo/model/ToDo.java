package com.norm.todo.model;

import org.springframework.data.annotation.Id;

public class ToDo {

    @Id
    private String id;

    private String task;

    private String description;

    public String getId() {
        return id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}