package com.norm.todo.repository;

import com.norm.todo.model.ToDo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ToDoRepository extends MongoRepository<ToDo, String> {

    //public ToDo findById(String id);

}